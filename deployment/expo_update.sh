echo "Updating app"

# We will do the update and take the output of the update into variable
update_app_message=$(EXPO_TOKEN=$EXPO_TOKEN npx eas update --branch "main" --non-interactive --auto)

update_app_link=$(echo "$update_app_message" | grep -o 'https:\/\/expo.dev\/accounts\/paprik\S*')

message="New update available 🚀: $update_app_link "

echo "Sending message to slack: "

echo "$message"

curl -X POST -H 'Content-type: application/json' --data '{"text":"'"$message"'"}' "https://hooks.slack.com/services/$SLACK_SECRET"

