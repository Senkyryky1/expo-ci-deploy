EXPO_ASC_API_KEY_PATH=$APP_STORE_CONNECT_API_KEY_KEY
EXPO_ASC_KEY_ID=$APP_STORE_CONNECT_API_KEY_KEY_ID
EXPO_ASC_ISSUER_ID=$APP_STORE_CONNECT_API_KEY_ISSUER_ID

eas build --profile planr-bhg-prod --platform all --non-interactive --auto-submit
eas build --profile reportr-bhg-prod --platform all --non-interactive --auto-submit

message="Reportr bhg and Planr bhg prod were built and submitted to stores (version $CI_COMMIT_TAG)"

echo "Sending message to slack: "

echo "$message"

curl -X POST -H 'Content-type: application/json' --data '{"text":"'"$message"'"}' "https://hooks.slack.com/services/$SLACK_SECRET"
