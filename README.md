## Expo CI Deploy test repo

To fulfil our customer needs in codeto, we realized we need automatic deploy process of app on changes, the reasons were:
- we don't have to ask developer to publish latest version manually, so it saves developer time
- we and our customers have constant overview of what is happening in latest development
- we wanted to have message about new versions in company channels

We will have a look how we faced this challenge using Gitlab. We will have a look on:

- Automated update of expo apps on merge to `main` branch and sending message about that
- Automated build and publish to stores and sending message about that

## Goals 

- update expo app on merge to `main` and send message to slack
- build and publish app to stores on tag and send message to slack

If you don't need to send message to slack, you can of course skip the related steps

We will show you step by step how to set everything up using demo app created with `npx create-expo-app --template`
In this guide, the web version is omitted. 

## Prerequisites

- Expo repo with [eas](https://expo.dev/eas), eas is in this case installed in dev modules (`npm i eas-cli -D`)
- Create expo project on expo.dev, in this case I have it under name `my-app`, name must correspond to `slug` in `app.json`
- You have to run now `eas init` configure EAS project and link it, `app.json` will be then updated with project id

- `EXPO_TOKEN` variable set on your Gitlab CI, on how to get one see: https://docs.expo.dev/accounts/programmatic-access/#bot-users-and-access-tokens
- `SLACK_SECRET` variable set on your Gitlab CI to get send message to your Slack workspace channel, on how to get one see: https://api.slack.com/authentication/token-types
How to set those variables, check this guide: https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui

### Preparing gitlab-ci.yml file

`gitlab-ci.yml` file serves as a configuration for Gitlab CI system. We have to prepare the npm install job which will
be used both for update and for build

```yaml
stages:
  - install

variables:
  NODE_VERSION: 14.21.3-alpine
  
install:
  stage: install
  image: "node:$NODE_VERSION"
  script:
    - "npm ci"
  artifacts:
    paths:
      - node_modules/
    expire_in: 30min
  only:
    - main
```

install job will now run on every merge to `main`, we have also added the part for caching node modules for 30min 
to save some installing when deploying frequently, that's the `artifacts` part. Note that we are using specific node version, you can 
use whatever version you like, but it has to be specified explicitly. 

Now let's move on to update.

### Updating apps on expo

Updating apps is done for both platforms in the same way

1. We will create `expo_update.sh` file in deployment folder in repo, instructions to commands are described in the comments in the code below:
    ```sh
    echo "Updating app"
   
    # Do the update and take the output of the update into variable, non interactive and auto flags are ensuring that CI system won't wait for any input from the user
    update_app_message=$(EXPO_TOKEN=$EXPO_TOKEN eas update --branch "main" --non-interactive --auto)
    
    # Parse the link from the message of our new update
    # ! Note here that in link you have to have your account name to parse the message correctly !
    update_app_link=$(echo "$update_app_message" | grep -o 'https:\/\/expo.dev\/accounts\/paprik\S*')
    
    # Prepare the message which will be sent to slack
    message="New update available 🚀: $update_app_link "
    
    echo "Sending message to slack: "
    echo "$message"
    
    # Send it to our slack workspace 
    curl -X POST -H 'Content-type: application/json' --data '{"text":"'"$message"'"}' "https://hooks.slack.com/services/$SLACK_SECRET"
    ```

2. Now, what is quite common is that gitlab-ci.yml won't find this file, so run the `git update-index --chmod=+x expo_update.sh` to make it executable

3. Open `gitlab-ci-yml` and let's add the update job
    ```yaml
    image: node:16.19.1
    
    stages:
     - install
     - update
    
    install:
      stage: install
      script:
       - "npm ci"
      artifacts:
        paths:
          - node_modules/
        expire_in: 30min
      only:
        - main
    
    update:
      stage: update
      needs: ["install"]
      script:
        - npm i eas-cli -g
        - apk update && apk add git && apk add curl
        - deployment/expo_update.sh
      only:
        - main
     ```
   - `npm i eas-cli -g` install eas for the CI container
   - the `apk update && apk add git && apk add curl` will install curl to send our Slack message
   - `deployment/expo_update.sh` will run our shell file which will do the update
   - Note the node version, in this case, I have used the one which I had on my machine 

4. It is good to test the update from your local machine to ensure that the node version on the Gitlab will work ok, sometimes some versions in combination some versions of expo are causing problems 

5. Now let's test it by merging something to `main`
 

### Building and submitting to stores

#### iOS

1. You have to create 



## Env variables set on gitlab

### For building and publishing
